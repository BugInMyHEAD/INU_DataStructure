/*
* 자료구조 (2018-1) 숙제 #3 2.
*
* 201202914 김건우
*/

#include <stdio.h>
#include <stdlib.h>

#include "!declaration.h"


const int arr[] = { 10, 15, 8, 18, 7, 1, 5, 9, 17, 11, 12, 30, 25, 6, 32, };


int intComparer(const void* a, const void* b)
{
	int aa = *(int*)a;
	int bb = *(int*)b;
	switch (( aa < bb ) << 1 | ( aa > bb ) << 0)
	{
	case 0b00:
		return 0;
	case 0b01:
		return 1;
	case 0b10:
		return -1;
	default:
		exit(6);
	}
}


int main(void)
{
	Heap_postfix heap; Heap_postfix_construct(&heap, intComparer);

	for (size_t i1 = 0; i1 < ARRLEN(arr); ++i1)
	{
		Heap_postfix_insert(&heap, arr[i1]);
	}

	/* Heap_delete는 Heap이 비어있으면 0을 반환 */
	for (int i1; Heap_postfix_delete(&heap, &i1); )
	{
		printf("%d ", i1);
	}
	puts("");

	Heap_postfix_destruct(&heap);

	return 0;
}
