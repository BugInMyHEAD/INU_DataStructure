/*
* 자료구조 (2018-1) 숙제 #2
*
* 201202914 김건우
*/

#pragma once


#include <malloc.h>
#include <memory.h>

#include "../AlgorithmStudying/customdef.h"


#define DECLARE_STACK(postfix, type)\
\
typedef struct Stack_##postfix Stack_##postfix;\
struct Stack_##postfix\
{\
	type * floor;\
	type * ceil;\
	type * top;\
};\
\
int Stack_##postfix##_init(Stack_##postfix* stack, size_t initialSpace);\
void Stack_##postfix##_destroy(Stack_##postfix* stack);\
int Stack_##postfix##_push(Stack_##postfix* stack, const type* data);\
int Stack_##postfix##_pop(Stack_##postfix* stack, type* data);\
int Stack_##postfix##_peek(const Stack_##postfix* stack, type* data);\
int Stack_##postfix##_isempty(const Stack_##postfix* stack);\


#define DEFINE_STACK(postfix, type)\
\
\
/**
 * 스택 초기화
 * 
 * @param initialSpace 초기 할당할 스택의 크기
 *                     1보다 크고 SIZE_MAX 보다 작아야 함
 * @return 0: 동적 할당 실패로 사용 불가 시,
 *            initialSpace < 1 인 경우
 *            initialSpace == SIZE_MAX 인 경우
 *         1: 초기화 성공
 */\
int Stack_##postfix##_init(Stack_##postfix* stack, size_t initialSpace)\
{\
	/* initialSpace < 1 또는 initialSpace == SIZE_MAX 인 경우
	 * 동적 할당된 floor 배열의 0번 인덱스는 사용되지 않음
	 */\
	size_t maspace = initialSpace + 1;\
	if (maspace < 2)\
	{\
		return 0;\
	}\
	\
	if (!MALLOC_ASSIGN(stack->floor, maspace))\
	{\
		return 0;\
	}\
	\
	stack->ceil = stack->floor + initialSpace;\
	stack->top = stack->floor;\
	\
	return 1;\
}\
\
\
/**
 * 스택 내부 동적 할당된 배열 메모리 반환
 */\
void Stack_##postfix##_destroy(Stack_##postfix* stack)\
{\
	FREE_ASSIGN(stack->floor);\
}\
\
\
/**
 * 스택의 맨 위에 data를 추가함
 * 
 * @return 동적 할당 실패로 push 불가 시 0, 그외는 1
 */\
int Stack_##postfix##_push(Stack_##postfix* stack, const type* data)\
{\
	/* 스택이 현재 가득 찬 경우 */\
	if (stack->top == stack->ceil)\
	{\
		ptrdiff_t newSpace = ( stack->ceil - stack->floor ) * 2;\
		/* 현재 공간 크기의 2배 할당 시도 */\
		type* newArr;\
		if (!REALLOC_ASSIGN(newArr, stack->floor, newSpace))\
		{\
			return 0;\
		}\
		\
		stack->ceil = newArr + newSpace;\
		stack->top += newArr - stack->floor;\
		stack->floor = newArr;\
	}\
	\
	*++stack->top = *data;\
	\
	return 1;\
}\
\
\
/**
 * 스택의 맨 위의 data를 삭제하고 반환
 * 
 * @param data pop한 data를 저장할 포인터. 실패 시 불변.
 * @return 비어 있어서 pop 불가 시 0, 그외는 1
 */\
int Stack_##postfix##_pop(Stack_##postfix* stack, type* data)\
{\
	if (Stack_##postfix##_isempty(stack))\
	{\
		return 0;\
	}\
	\
	*data = *stack->top--;\
	\
	return 1;\
}\
\
\
/**
 * 스택의 맨 위의 data를 반환
 * 스택의 내용 불변
 * 
 * @param data peek한 data를 저장할 포인터. 실패 시 불변.
 * @return 비어 있어서 peek 불가 시 0, 그외는 1
 */\
int Stack_##postfix##_peek(const Stack_##postfix* stack, type* data)\
{\
	if (Stack_##postfix##_isempty(stack))\
	{\
		return 0;\
	}\
	\
	*data = *stack->top;\
	\
	return 1;\
}\
\
\
/**
 * 스택이 비어있는지 여부를 반환
 * 
 * @return 비어 있으면 1, 그외는 0
 */\
int Stack_##postfix##_isempty(const Stack_##postfix* stack)\
{\
	return stack->top == stack->floor;\
}\

