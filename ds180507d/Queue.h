/*
* 자료구조 (2018-1) 숙제 #2
*
* 201202914 김건우
*/

#pragma once


#include <malloc.h>
#include <memory.h>

#include "../AlgorithmStudying/customdef.h"


#define DECLARE_QUEUE(postfix, type)\
\
typedef struct Queue_##postfix Queue_##postfix;\
struct Queue_##postfix\
{\
	type * arr;\
	size_t space;\
	size_t front, rear;\
};\
\
int Queue_##postfix##_init(Queue_##postfix* queue, size_t initialSpace);\
void Queue_##postfix##_destroy(Queue_##postfix* queue);\
int Queue_##postfix##_enqueue(Queue_##postfix* queue, const type* data);\
int Queue_##postfix##_dequeue(Queue_##postfix* queue, type* data);\
int Queue_##postfix##_peek(const Queue_##postfix* queue, type* data);\
int Queue_##postfix##_isempty(const Queue_##postfix* queue);\


#define DEFINE_QUEUE(postfix, type)\
\
\
/**
 * 큐 초기화
 * 
 * @param initialSpace 초기 할당할 큐의 크기
 *                     1보다 크고 SIZE_MAX 보다 작아야 함
 * @return 0: 동적 할당 실패로 사용 불가 시,
 *            initialSpace < 1 인 경우
 *            initialSpace == SIZE_MAX 인 경우
 *         1: 초기화 성공
 */\
int Queue_##postfix##_init(Queue_##postfix* queue, size_t initialSpace)\
{\
	/* initialSpace < 1 또는 initialSpace == SIZE_MAX 인 경우
	 * 동적 할당된 arr 배열의 한 개 요소의 공간은 사용되지 않음
	 */\
	if (++initialSpace < 2)\
	{\
		return 0;\
	}\
	\
	if (!MALLOC_ASSIGN(queue->arr, initialSpace))\
	{\
		return 0;\
	}\
	\
	queue->space = initialSpace;\
	queue->front = queue->rear = 0;\
	\
	return 1;\
}\
\
\
/**
 * 큐 내부 동적 할당된 배열 메모리 반환
 */\
void Queue_##postfix##_destroy(Queue_##postfix* queue)\
{\
	FREE_ASSIGN(queue->arr);\
}\
\
\
/**
 * 큐의 맨 뒤에 data를 추가함
 * 
 * @return 동적 할당 실패로 enqueue 불가 시 0, 그외는 1
 */\
int Queue_##postfix##_enqueue(Queue_##postfix* queue, const type* data)\
{\
	size_t nextIdx = ( queue->rear + 1 ) % queue->space;\
	/* 큐가 현재 가득 찬 경우 */\
	if (nextIdx == queue->front)\
	{\
		size_t newSpace = queue->space * 2;\
		/* 현재 공간 크기의 2배 할당 시도 */\
		type* newArr;\
		if (!REALLOC_ASSIGN(newArr, queue->arr, newSpace))\
		{\
			return 0;\
		}\
		\
		/**
		 * 큐의 내용이 중간에 끊어진 경우 공간이 확장되었으므로
		 * 복사하여 붙임
		 */\
		if (queue->rear < queue->front)\
		{\
			memcpy(&newArr[queue->space],\
					&newArr[0],\
					sizeof(*newArr) * queue->rear);\
			queue->rear += queue->space;\
		}\
		queue->arr = newArr;\
		queue->space = newSpace;\
		/* 공간이 확장되었으므로 modulo 연산 필요 없음 */\
		nextIdx = queue->rear + 1;\
	}\
	\
	queue->arr[queue->rear] = *data;\
	queue->rear = nextIdx;\
	\
	return 1;\
}\
\
\
/**
 * 큐의 맨 앞의 data를 삭제하고 반환
 * 
 * @param data dequeue한 data를 저장할 포인터. 실패 시 불변.
 * @return 비어 있어서 dequeue 불가 시 0, 그외는 1
 */\
int Queue_##postfix##_dequeue(Queue_##postfix* queue, type* data)\
{\
	if (Queue_##postfix##_isempty(queue))\
	{\
		return 0;\
	}\
	\
	*data = queue->arr[queue->front];\
	queue->front = ( queue->front + 1 ) % queue->space;\
	\
	return 1;\
}\
\
\
/**
 * 큐의 맨 앞의 data를 반환
 * 큐의 내용 불변
 * 
 * @param data peek한 data를 저장할 포인터. 실패 시 불변.
 * @return 비어 있어서 peek 불가 시 0, 그외는 1
 */\
int Queue_##postfix##_peek(const Queue_##postfix* queue, type* data)\
{\
	if (Queue_##postfix##_isempty(queue))\
	{\
		return 0;\
	}\
	\
	*data = queue->arr[queue->front];\
	\
	return 1;\
}\
\
\
/**
* 큐가 비어있는지 여부를 반환
* 
* @return 비어 있으면 1, 그외는 0
*/\
int Queue_##postfix##_isempty(const Queue_##postfix* queue)\
{\
	return queue->front == queue->rear;\
}\

