/*
 * 자료구조 (2018-1) 숙제 #2
 *
 * 201202914 김건우
 */

#include <stdio.h>

#include "Queue.h"
#include "Stack.h"

#include "../AlgorithmStudying/customdef.h"
#include "../AlgorithmStudying/CDataStructure/LinkedList.h"


DECLARE_LLIST(int, int)

DECLARE_QUEUE(int, int)
DEFINE_QUEUE(int, int)

DECLARE_STACK(int, int)
DEFINE_STACK(int, int)

int primeList(LList_int*, int);


int main(void)
{
	int minin = 100;
	printf("%d 이상의 정수 입력: ", minin);
	char buf[BUFSIZ];
	gets_s(buf, ARRLEN(buf));
	int n;
	if (1 != sscanf_s(buf, "%d", &n) || n < minin)
	{
		printf("100 이상의 정수 입력을 받지 못하여 종료합니다.\n");
		goto __exit;
	}

	int initialSpace = 20;
	Queue_int queue; Queue_int_init(&queue, initialSpace);
	Stack_int stack; Stack_int_init(&stack, initialSpace);

	int bound = 10;

	/* 1부터 bound까지는 무조건 넣을 수 있음 */
	for (int i1 = 1; i1 < bound; ++i1)
	{
		Queue_int_enqueue(&queue, &i1);
		Stack_int_push(&stack, &i1);
	}

	/* 소수들의 리스트를 만듦 */
	LList_int list; LList_int_init(&list);
	int noOfPrime = primeList(&list, n);
	LList_int_Iter iter; LList_int_iterHead(&list, &iter);

	for (int i1 = bound, prime = 0; i1 <= n; ++i1)
	{
		/*
		 * i1의 값마다 매번 소수들의 리스트를 순회하여 소수임을 판별하는 것이
		 * 아님. 소수들의 리스트는 오름차순이므로, i1보다 크거나 같은 소수 중
		 * 가장 작은 수를 얻어올 수 있음.
		 * LListGoRelFwd는 범위를 벗어난 인덱스에 대해서 실패하므로 prime이
		 * head node나 그외 다른 메모리 위치에서 쓰레기값을 받지 않음.
		 * 따라서, 루프의 후반부에서 prime이 prime <= n 인 수 중 가장 큰 소수가
		 * 되면, 더 이상 바뀌지 않음.
		 */
		while (i1 > prime && LList_int_goRelFwd(&iter, 1))
		{
			prime = LList_int_getData(&iter);
		}
		if (i1 == prime)
		{
			printf("소수:    %d\n", i1);
			int i4;
			Queue_int_dequeue(&queue, &i4);
			printf("Dequeue: %d\n", i4);
			Stack_int_pop(&stack, &i4);
			printf("Pop:     %d\n", i4);
		}
		else
		{
			printf("합성수:  %d\n", i1);
			Queue_int_enqueue(&queue, &i1);
			Stack_int_push(&stack, &i1);
		}
		puts("");
	}

	Queue_int_destroy(&queue);
	Stack_int_destroy(&stack);
	LList_int_iterUnref(&iter);

__exit:;

	return 0;
}
