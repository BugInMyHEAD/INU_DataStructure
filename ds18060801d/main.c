/*
* 자료구조 (2018-1) 숙제 #3 1.
*
* 201202914 김건우
*/

#include <stdio.h>
#include <stdlib.h>

#include "../AlgorithmStudying/customdef.h"
#include "../AlgorithmStudying/CDataStructure/BinarySearchTree.h"


const int arr[] = { 10, 15, 8, 18, 7, 1, 5, 9, 17, 11, 12, 30, 25, 6, 32, };


int intComparer(int a, int b)
{
	switch (( a < b ) << 1 | ( a > b ) << 0)
	{
	case 0b00:
		return 0;
	case 0b01:
		return 1;
	case 0b10:
		return -1;
	default:
		exit(6);
	}
}


void intPrinter(int i)
{
	printf("%d ", i);
}


int main(void)
{
	int i0;
	BinarySearchTree bst; BST_construct(&bst, intComparer);
	
	for (size_t i1 = 0; i1 < ARRLEN(arr); ++i1)
	{
		/* 메모리 부족 예외 처리 */
		if (BST_insert(&bst, arr[i1]) < 0)
		{
			printf("error\n");
			goto gotolExit;
		}
	}

	BinaryTree_destroyNode(BST_delete(&bst, 9));
	BinaryTree_destroyNode(BST_delete(&bst, 7));
	BinaryTree_destroyNode(BST_delete(&bst, 10));

	/* 리프 노드 순회 및 출력 후 총 리프 노드 수 출력 */
	i0 = BST_traverse(&bst, BinaryTree_travLeaf, intPrinter);
	printf("\nLeaf total: %d\n", i0);

	/* 중위 순회 및 출력 후 총 노드 수 출력 */
	i0 = BST_traverse(&bst, BinaryTree_travIn, intPrinter);
	printf("\nInorder total: %d\n", i0);

gotolExit:;
	BST_destruct(&bst);

	return 0;
}
